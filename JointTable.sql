SELECT 
 loans.id AS load_id, 
 loans.amount AS loan_amount, 
 users.id AS user_id, 
 users.created AS user_created, 
 transaction_fee,
 t_type 
FROM ((users
INNER JOIN loans ON users.id = loans.user_id)
INNER JOIN transactions ON loans.id = transactions.loan_id)
WHERE t_type = 'disbursement' ;




