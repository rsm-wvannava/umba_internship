SELECT
 EXTRACT(YEAR FROM TO_DATE(created, 'YYYY-MM-DD HH24:MI:SS')) AS created_year,
 EXTRACT(MONTH FROM TO_DATE(created, 'YYYY-MM-DD HH24:MI:SS')) AS created_month,
 COUNT (loans.id) AS number_of_loan,
 SUM (loans.amount) AS amount_of_loan,
 AVG (loans.amount) AS average_amount_of_loan,
 MIN (loans.amount) AS min_loan_size,
 MAX (loans.amount) AS max_loan_size 
FROM loans
GROUP BY created_year, created_month
ORDER BY created_year DESC, created_month DESC;