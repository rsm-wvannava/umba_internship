# __UMBA Internship__

The repository consists of three main sections

**1. Join table PostgreSQL coding**

Three tables has been joined by inner join function.
Please see workdone in "JointTable.sql"

**2. Aggregation PostgreSQL coding**

With the assumption of no filter for Loan status, data will be transformed and show by month year format.
Please see workdone in "Aggregation.sql"

**3. Weekly login R analysis**

Please see workdone in "weekly_logins.html" and "weekly_logins.Rmd"



